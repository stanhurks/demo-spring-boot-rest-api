package it.hurks.dao;

import it.hurks.model.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Data Access Object class for the Payment model.
 *
 * @author Stan
 */
@Transactional
public interface PaymentDao extends CrudRepository<Payment, Long> {

}
