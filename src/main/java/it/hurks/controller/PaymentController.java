package it.hurks.controller;

import it.hurks.dao.PaymentDao;
import it.hurks.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller for all payment related requests
 */
@RestController
public class PaymentController {

    @Autowired
    private PaymentDao paymentDao;

    @RequestMapping(value = "/payment/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> detailPayment (@PathVariable long id) {
        Payment payment;
        try {
            payment = paymentDao.findOne(id);
            return new ResponseEntity<>(payment, HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
